# MAPSDK

[![CI Status](https://img.shields.io/travis/YallowApp/MAPSDK.svg?style=flat)](https://travis-ci.org/YallowApp/MAPSDK)
[![Version](https://img.shields.io/cocoapods/v/MAPSDK.svg?style=flat)](https://cocoapods.org/pods/MAPSDK)
[![License](https://img.shields.io/cocoapods/l/MAPSDK.svg?style=flat)](https://cocoapods.org/pods/MAPSDK)
[![Platform](https://img.shields.io/cocoapods/p/MAPSDK.svg?style=flat)](https://cocoapods.org/pods/MAPSDK)

## Installation

MAPSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:


`pod 'MAPSDK' , :git => 'https://gitlab.com/mohammad.a.rababah/mapsdk-ios.git'`


then do not forget to run ```pod install``` in your terminal

## AppDeleget setup
in your AppDeleget class add these line of code 



``` swift
import MAPSDK
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, YallowMapSDKDeleget {
    val apiKey = "YOUR_API_KEY"
    func onInitSuccessfully() {
        // mapsdk init Successfully
    }
    
    func onInitFail(message: String) {
        // mapsdk init fail
    }
    



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        //....
        YallowTrackingMapView.initMap(apiKey: apiKey, yallowDeleget: self)
        return true
    }
```




## Design 
in your nib or storybord file add `UIView` that will hold the map tracking info and add class for the view as `YallowTrackingMapView`

![](Example/images/1.png)

## Tracking ViewController
in your tracking  ViewController add these line of codes

```swift
import MAPSDK

class TrackingViewController: UIViewController, YallowOrderDeleget {
    func onDriverLocationUpdate(location: CLLocationCoordinate2D) {
        // read that driver location change
    }
    func onOrderUpdate(order: OrderResponse) {
     // read that order values change such as status , price , ....etc
    }
    
    func onOrderFail(message: String) {
        //reading order fail usually occurs because the order id is wrong
    }
    
    @IBOutlet weak var yallowMap: YallowTrackingMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //....
         self.yallowMap.trackOrder(orderId: YOUR_ORDER_ID, yallowOrderDeleget: self)
         //....   
     
      
      
    
    
    }

```

to enable and disable read marker from url you can this code. the defualt is on (optional)

 ` self.yallowMap.showMarkerfromUrl = true`

remember that our tracking map is Google map and you can always add your own customization for it such as
        
```swift
        self.yallowMap.settings.zoomGestures = true
        self.yallowMap.settings.myLocationButton = true
        self.yallowMap.settings.scrollGestures = true
```
for more customization you can read google map documentation at this [link](https://developers.google.com/maps/documentation/ios-sdk/start)

## Author

YallowApp, mohammad.a.rababah@gmail.com

## License

MAPSDK is available under the MIT license. See the LICENSE file for more info.
