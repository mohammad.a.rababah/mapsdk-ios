//
//  MapResfullApi.swift
//  MapTest
//
//  Created by Tikino Pawn on 8/10/20.
//  Copyright © 2020 Yallow. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class MapResfullApi {
    private static let mapResfullApi = MapResfullApi()
        static let BASE_URL =  "https://api.yallow.com/a/"
    private init(){}
       
       static func  getIntsance()-> MapResfullApi{
        return MapResfullApi.mapResfullApi
       }
    func loadBrand(apiKey : String,loginResponse:@escaping (DataResponse<Brand>) -> Void ){
        Alamofire.request(MapResfullApi.BASE_URL + apiKey + "/branding" , method: .get, parameters: nil, headers:  nil).responseObject (completionHandler:loginResponse )
    }
    

    func loadOrderDetails(apiKey : String, order_id : String,loginResponse:@escaping (DataResponse<OrderResponse>) -> Void ){
      Alamofire.request(MapResfullApi.BASE_URL + apiKey + "/order/get_full/" + order_id , method: .get, parameters: nil, headers:  nil).responseObject (completionHandler:loginResponse )
  }
}
