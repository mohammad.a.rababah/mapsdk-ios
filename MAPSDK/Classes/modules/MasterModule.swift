//
//  MasterModule.swift
//  Driver
//
//  Created by Tikino Pawn on 6/11/19.
//  Copyright © 2019 Yallow. All rights reserved.
//

import Foundation
import ObjectMapper
public class  MasterModule: NSObject,Mappable {
    override  init() {
        
    }
    public required init?(map: Map) {
        
    }
    
    public  func mapping(map: Map) {
        message <- map["message"]
    }
  public   var postion = -1
  public   var isExpand = false
   public  var extraString = ""
  public   var message = Message()
  public   func toString()->String{
        return ""
    }
    
    func formatPrice(price: Double) -> String{
      return NSLocalizedString("QAR", comment: "") + " " + String(format:"%.2f", price)
    }
    
}
   public  class Message :NSObject , Mappable {
    func encode(with coder: NSCoder) {
        
    }
    
    required init?(coder: NSCoder) {
        super.init()
    }
    
   public  var en:String? = ""
  public   var ar:String? = ""

   public  override init() {
        super.init()
        
    }
    
    public required init?(map: Map) {
    }
    
    
   public   func mapping(map: Map) {
        en <- map["en"]
        ar <- map["ar"]
    }
    
    
}
