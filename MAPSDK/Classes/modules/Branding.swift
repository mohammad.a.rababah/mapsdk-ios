//
//  Brand.swift
//  MapTest
//
//  Created by Tikino Pawn on 8/10/20.
//  Copyright © 2020 Yallow. All rights reserved.
//

import Foundation
import ObjectMapper
public class Brand : MasterModule , NSCoding{
   public required convenience init?(coder: NSCoder) {
            self.init()
      logo =  coder.decodeObject(forKey: "logo") as! String
    splash_text_color  = coder.decodeObject(forKey: "logo") as! String
            splash_text  = coder.decodeObject(forKey: "splash_text") as! String
            splash_screen  = coder.decodeObject(forKey: "splash_screen") as! String
            app_color_1  = coder.decodeObject(forKey: "app_color_1") as! String
            app_color_2  = coder.decodeObject(forKey: "app_color_2") as! String
            app_color_3  = coder.decodeObject(forKey: "app_color_3") as! String
            text_color_1  = coder.decodeObject(forKey: "text_color_1") as! String
            text_color_2 = coder.decodeObject(forKey: "text_color_2") as! String
            text_color_3 = coder.decodeObject(forKey: "text_color_3") as! String
            text_color_4 = coder.decodeObject(forKey: "text_color_4") as! String
            FB_application_Id = coder.decodeObject(forKey: "FB_application_Id") as! String
            FB_api_key = coder.decodeObject(forKey: "FB_api_key") as! String
            FB_database_url = coder.decodeObject(forKey: "FB_database_url") as! String
    }
    public override init() {
        super.init()
    }
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(logo, forKey: "logo")
        aCoder.encode(splash_text_color, forKey: "splash_text_color")
        aCoder.encode(splash_text, forKey: "splash_text")
        aCoder.encode(splash_screen, forKey: "splash_screen")
        aCoder.encode(app_color_1, forKey: "app_color_1")
        aCoder.encode(app_color_2, forKey: "app_color_2")
        aCoder.encode(app_color_3, forKey: "app_color_3")
        aCoder.encode(text_color_1, forKey: "text_color_1")
        aCoder.encode(text_color_2, forKey: "text_color_2")
        aCoder.encode(text_color_3, forKey: "text_color_3")
        aCoder.encode(text_color_4, forKey: "text_color_4")
        aCoder.encode(FB_application_Id, forKey: "FB_application_Id")
        aCoder.encode(FB_api_key, forKey: "FB_api_key")
        aCoder.encode(FB_database_url, forKey: "FB_database_url")
        
    }
    
    public var  logo:String = ""
    public var splash_text_color:String = ""
    public var splash_text:String = ""
    public var splash_screen:String = ""
    public var app_color_1:String = ""
    public var app_color_2:String = ""
    public var app_color_3:String = ""
    public var text_color_1:String  = ""
    public var text_color_2:String  = ""
    public var text_color_3:String = ""
    public var text_color_4:String = ""
    public var FB_application_Id:String = "1:766116894286:android:dac49f02339ca60a"
    public var FB_api_key:String = "AIzaSyBt7Xf6tWjXie986TL0uwcqEERFHo3MspM"
    public var FB_database_url:String = "https://yallow-line.firebaseio.com/"
   public override func mapping(map: Map) {
        logo  <- map["logo"]
         splash_text_color  <- map[ "splash_text_color"]
                 splash_text  <- map[ "splash_text"]
                 splash_screen  <- map[ "splash_screen"]
                 app_color_1  <- map[ "app_color_1"]
                 app_color_2   <- map[ "app_color_2"]
                 app_color_3  <- map[ "app_color_3"]
                 text_color_1   <- map[ "text_color_1"]
                 text_color_2 <- map[ "text_color_2"]
                 text_color_3 <- map[ "text_color_3"]
                 text_color_4 <- map[ "text_color_4"]
                 FB_application_Id <- map[ "FB_application_Id"]
                 FB_api_key <- map[ "FB_api_key"]
                 FB_database_url  <- map[ "FB_database_url"]
    }
}
