/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

public struct OrderResponse : Mappable {
	public var id : Int?
	public var status_id : Int?
	public var account_id : Int?
	public var api_sender_id : String?
	public var tracking_id : String?
	public var trip_id : Int?
	public var city_id : String?
	public var client_order_id : String?
	public var delivery_order_id : String?
	public var driver_id : Int?
	public var vehicle_id : Int?
	public var pickup_id : Int?
	public var pickup_address_id : Int?
	public var customer_id : Int?
	public var customer_address_id : Int?
	public var shipping_type_id : Int?
	public var customer_alias : String?
	public var details : String?
	public var currency : String?
	public var fees : String?
	public var before_tax_fees : Int?
	public var confirm_fees : Int?
	public var confirm_fees_calc : Int?
	public var pre_confirm_fees : Int?
	public var pre_confirm_fees_calc : Int?
	public var confirm_fees_set : Int?
	public var fees_calc : String?
	public var driver_fees : String?
	public var driver_percentage : Int?
	public var collection_amount : Int?
	public var account_partial_pay : String?
	public var paid : Int?
	public var partially_paid : Int?
	public var value : String?
	public var number_of_items : Int?
	public var billable : Int?
	public var responsible : String?
	public var action_type : String?
	public var action_amount : Int?
	public var action_reason : String?
	public var payment_method_id : Int?
	public var start_at : Start_at?
	public var request_deliver_at : String?
	public var delivery_diff : Int?
	public var broadcast_delay : Int?
	public var calc_broadcast_delay : Int?
	public var broadcast_at : String?
	public var acceptance_buffer : Int?
	public var expected_accept_at : String?
	public var accepted_at : String?
	public var expected_pickup : String?
	public var pickup_buffer : Int?
	public var expected_at_pickup : String?
	public var at_pickup_at : String?
	public var pickup_handling_time : Int?
	public var pickup_at : String?
	public var expected_delivery : String?
	public var dropoff_buffer : Int?
	public var expected_at_dropoff : String?
	public var at_dropoff_at : String?
	public var dropoff_handling_time : Int?
	public var dropoff_at : String?
	public var preperation_time : Int?
	public var hidden : Int?
	public var type : String?
	public var api_order : Int?
	public var duration : Int?
	public var distance : Double?
	public var pickup_distance : Double?
	public var pickup_duration : Int?
	public var arrive_in : Int?
	public var arrive_in_at : String?
	public var confirm_duration : Int?
	public var confirm_distance : Int?
	public var pre_confirm_duration : Int?
	public var pre_confirm_distance : Int?
	public var rated : Int?
	public var sms : Int?
	public var tracked : Int?
	public var round : Int?
	public var pause : Int?
	public var round_set : Int?
	public var lock : Int?
	public var lock_by : String?
	public var fail_attempts : Int?
	public var fail_reason : String?
	public var cancel_reason : String?
	public var marketplace_business_start : String?
	public var marketplace_business_end : String?
	public var marketplace_business_day : String?
	public var marketplace_business_date : String?
	public var service_business_start : String?
	public var service_business_end : String?
	public var service_business_day : String?
	public var service_business_date : String?
	public var branch_business_start : String?
	public var branch_business_end : String?
	public var branch_business_day : String?
	public var branch_business_date : String?
	public var barcode : String?
	public var qrcode : String?
	public var total_assigned : Int?
	public var total_accepted : Int?
	public var total_rejected : Int?
	public var total_unassigned : Int?
	public var created_at : String?
	public var updated_at : String?
	public var deleted_at : String?
	public var expected_delivery_timer : Expected_delivery_timer?
	public var expected_pickup_timer : Expected_pickup_timer?
	public var expected_delivery_milliseconds : Int?
	public var expected_pickup_milliseconds : Int?
	public var pure_expected_pickup_timer : Pure_expected_pickup_timer?
	public var pure_expected_pickup : Pure_expected_pickup?
	public var expire_in : Int?
	public var expire_time : String?
	public var dropped_at_timer : Dropped_at_timer?
	public var prepared_at : Prepared_at?
	public var preparation_timer : Preparation_timer?
	public var broadcast_timer : Broadcast_timer?
	public var pickup_in_timer : Pickup_in_timer?
	public var is_preorder : Bool?
	public var preorder_type : String?
	public var dropped_at_duration : Dropped_at_duration?
	public var required_fees : String?
	public var payment_type : String?
	public var stage : Int?
	public var user : User?
	public var status : Status?
	public var driver : Driver?
	public var vehicle : Vehicle?
	public var client : Client?
	public var branch : Branch?
	public var customer : Customer?
	public var customer_address : Customer_address?
	public var payment_method : Payment_method?

	public init?(map: Map) {

	}

	public mutating func mapping(map: Map) {

		id <- map["id"]
		status_id <- map["status_id"]
		account_id <- map["account_id"]
		api_sender_id <- map["api_sender_id"]
		tracking_id <- map["tracking_id"]
		trip_id <- map["trip_id"]
		city_id <- map["city_id"]
		client_order_id <- map["client_order_id"]
		delivery_order_id <- map["delivery_order_id"]
		driver_id <- map["driver_id"]
		vehicle_id <- map["vehicle_id"]
		pickup_id <- map["pickup_id"]
		pickup_address_id <- map["pickup_address_id"]
		customer_id <- map["customer_id"]
		customer_address_id <- map["customer_address_id"]
		shipping_type_id <- map["shipping_type_id"]
		customer_alias <- map["customer_alias"]
		details <- map["details"]
		currency <- map["currency"]
		fees <- map["fees"]
		before_tax_fees <- map["before_tax_fees"]
		confirm_fees <- map["confirm_fees"]
		confirm_fees_calc <- map["confirm_fees_calc"]
		pre_confirm_fees <- map["pre_confirm_fees"]
		pre_confirm_fees_calc <- map["pre_confirm_fees_calc"]
		confirm_fees_set <- map["confirm_fees_set"]
		fees_calc <- map["fees_calc"]
		driver_fees <- map["driver_fees"]
		driver_percentage <- map["driver_percentage"]
		collection_amount <- map["collection_amount"]
		account_partial_pay <- map["account_partial_pay"]
		paid <- map["paid"]
		partially_paid <- map["partially_paid"]
		value <- map["value"]
		number_of_items <- map["number_of_items"]
		billable <- map["billable"]
		responsible <- map["responsible"]
		action_type <- map["action_type"]
		action_amount <- map["action_amount"]
		action_reason <- map["action_reason"]
		payment_method_id <- map["payment_method_id"]
		start_at <- map["start_at"]
		request_deliver_at <- map["request_deliver_at"]
		delivery_diff <- map["delivery_diff"]
		broadcast_delay <- map["broadcast_delay"]
		calc_broadcast_delay <- map["calc_broadcast_delay"]
		broadcast_at <- map["broadcast_at"]
		acceptance_buffer <- map["acceptance_buffer"]
		expected_accept_at <- map["expected_accept_at"]
		accepted_at <- map["accepted_at"]
		expected_pickup <- map["expected_pickup"]
		pickup_buffer <- map["pickup_buffer"]
		expected_at_pickup <- map["expected_at_pickup"]
		at_pickup_at <- map["at_pickup_at"]
		pickup_handling_time <- map["pickup_handling_time"]
		pickup_at <- map["pickup_at"]
		expected_delivery <- map["expected_delivery"]
		dropoff_buffer <- map["dropoff_buffer"]
		expected_at_dropoff <- map["expected_at_dropoff"]
		at_dropoff_at <- map["at_dropoff_at"]
		dropoff_handling_time <- map["dropoff_handling_time"]
		dropoff_at <- map["dropoff_at"]
		preperation_time <- map["preperation_time"]
		hidden <- map["hidden"]
		type <- map["type"]
		api_order <- map["api_order"]
		duration <- map["duration"]
		distance <- map["distance"]
		pickup_distance <- map["pickup_distance"]
		pickup_duration <- map["pickup_duration"]
		arrive_in <- map["arrive_in"]
		arrive_in_at <- map["arrive_in_at"]
		confirm_duration <- map["confirm_duration"]
		confirm_distance <- map["confirm_distance"]
		pre_confirm_duration <- map["pre_confirm_duration"]
		pre_confirm_distance <- map["pre_confirm_distance"]
		rated <- map["rated"]
		sms <- map["sms"]
		tracked <- map["tracked"]
		round <- map["round"]
		pause <- map["pause"]
		round_set <- map["round_set"]
		lock <- map["lock"]
		lock_by <- map["lock_by"]
		fail_attempts <- map["fail_attempts"]
		fail_reason <- map["fail_reason"]
		cancel_reason <- map["cancel_reason"]
		marketplace_business_start <- map["marketplace_business_start"]
		marketplace_business_end <- map["marketplace_business_end"]
		marketplace_business_day <- map["marketplace_business_day"]
		marketplace_business_date <- map["marketplace_business_date"]
		service_business_start <- map["service_business_start"]
		service_business_end <- map["service_business_end"]
		service_business_day <- map["service_business_day"]
		service_business_date <- map["service_business_date"]
		branch_business_start <- map["branch_business_start"]
		branch_business_end <- map["branch_business_end"]
		branch_business_day <- map["branch_business_day"]
		branch_business_date <- map["branch_business_date"]
		barcode <- map["barcode"]
		qrcode <- map["qrcode"]
		total_assigned <- map["total_assigned"]
		total_accepted <- map["total_accepted"]
		total_rejected <- map["total_rejected"]
		total_unassigned <- map["total_unassigned"]
		created_at <- map["created_at"]
		updated_at <- map["updated_at"]
		deleted_at <- map["deleted_at"]
		expected_delivery_timer <- map["expected_delivery_timer"]
		expected_pickup_timer <- map["expected_pickup_timer"]
		expected_delivery_milliseconds <- map["expected_delivery_milliseconds"]
		expected_pickup_milliseconds <- map["expected_pickup_milliseconds"]
		pure_expected_pickup_timer <- map["pure_expected_pickup_timer"]
		pure_expected_pickup <- map["pure_expected_pickup"]
		expire_in <- map["expire_in"]
		expire_time <- map["expire_time"]
		dropped_at_timer <- map["dropped_at_timer"]
		prepared_at <- map["prepared_at"]
		preparation_timer <- map["preparation_timer"]
		broadcast_timer <- map["broadcast_timer"]
		pickup_in_timer <- map["pickup_in_timer"]
		is_preorder <- map["is_preorder"]
		preorder_type <- map["preorder_type"]
		dropped_at_duration <- map["dropped_at_duration"]
		required_fees <- map["required_fees"]
		payment_type <- map["payment_type"]
		stage <- map["stage"]
		user <- map["user"]
		status <- map["status"]
		driver <- map["driver"]
		vehicle <- map["vehicle"]
		client <- map["client"]
		branch <- map["branch"]
		customer <- map["customer"]
		customer_address <- map["customer_address"]
		payment_method <- map["payment_method"]
	}

}
