/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

public struct Data : Mappable {
	public var y : Int?
	public var m : Int?
	public var d : Int?
	public var h : Int?
	public var i : Int?
	public var s : Int?
	public var f : Double?
	public var weekday : Int?
	public var weekday_behavior : Int?
	public var first_last_day_of : Int?
	public var invert : Int?
	public var days : Int?
	public var special_type : Int?
	public var special_amount : Int?
	public var have_weekday_relative : Int?
	public var have_special_relative : Int?

public 	init?(map: Map) {

	}

public 	mutating func mapping(map: Map) {

		y <- map["y"]
		m <- map["m"]
		d <- map["d"]
		h <- map["h"]
		i <- map["i"]
		s <- map["s"]
		f <- map["f"]
		weekday <- map["weekday"]
		weekday_behavior <- map["weekday_behavior"]
		first_last_day_of <- map["first_last_day_of"]
		invert <- map["invert"]
		days <- map["days"]
		special_type <- map["special_type"]
		special_amount <- map["special_amount"]
		have_weekday_relative <- map["have_weekday_relative"]
		have_special_relative <- map["have_special_relative"]
	}

}
