/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

public  struct Client : Mappable {
	public var id : Int?
	public var ingr_id : String?
	public var ingr_source : String?
	public var group_id : Int?
	public var user_id : Int?
	public var team_leader_id : Int?
	public var country_id : Int?
	public var city_id : Int?
	public var name : String?
	public var phone : String?
	public var default_preparation_time : Int?
	public var min_preperation_time : Int?
	public var default_delivery_time : Int?
	public var delivery_fees : Int?
	public var min_order_amount : Int?
	public var enabled : Int?
	public var currency : String?
	public var no_address : Int?
	public var only_wallet : Int?
	public var no_name : Int?
	public var no_customer : Int?
	public var address_search : Int?
	public var allow_partial_pay : Int?
	public var partial_pay : String?
	public var show_cancel : Int?
	public var show_fees : Int?
	public var show_ref_order_id : Int?
	public var show_comment : Int?
	public var show_number_of_items : Int?
	public var show_order_value : Int?
	public var show_payment_methods : Int?
	public var enable_cod : Int?
	public var maps_key : String?
	public var pickup_handling_time : Int?
	public var dropoff_handling_time : Int?
	public var buffer_time : Int?
	public var api_key : String?
	public var app_ver : String?
	public var reference_id : String?
	public var note : String?
	public var enabled_at : String?
	public var disabled_at : String?
	public var deleted_at : String?
	public var created_at : String?
	public var updated_at : String?
	public var logo : String?
	public var balance : Int?
	public var group : Group?
	public var marker : String?

	public init?(map: Map) {

	}

	public mutating func mapping(map: Map) {

		id <- map["id"]
		ingr_id <- map["ingr_id"]
		ingr_source <- map["ingr_source"]
		group_id <- map["group_id"]
		user_id <- map["user_id"]
		team_leader_id <- map["team_leader_id"]
		country_id <- map["country_id"]
		city_id <- map["city_id"]
		name <- map["name"]
		phone <- map["phone"]
		default_preparation_time <- map["default_preparation_time"]
		min_preperation_time <- map["min_preperation_time"]
		default_delivery_time <- map["default_delivery_time"]
		delivery_fees <- map["delivery_fees"]
		min_order_amount <- map["min_order_amount"]
		enabled <- map["enabled"]
		currency <- map["currency"]
		no_address <- map["no_address"]
		only_wallet <- map["only_wallet"]
		no_name <- map["no_name"]
		no_customer <- map["no_customer"]
		address_search <- map["address_search"]
		allow_partial_pay <- map["allow_partial_pay"]
		partial_pay <- map["partial_pay"]
		show_cancel <- map["show_cancel"]
		show_fees <- map["show_fees"]
		show_ref_order_id <- map["show_ref_order_id"]
		show_comment <- map["show_comment"]
		show_number_of_items <- map["show_number_of_items"]
		show_order_value <- map["show_order_value"]
		show_payment_methods <- map["show_payment_methods"]
		enable_cod <- map["enable_cod"]
		maps_key <- map["maps_key"]
		pickup_handling_time <- map["pickup_handling_time"]
		dropoff_handling_time <- map["dropoff_handling_time"]
		buffer_time <- map["buffer_time"]
		api_key <- map["api_key"]
		app_ver <- map["app_ver"]
		reference_id <- map["reference_id"]
		note <- map["note"]
		enabled_at <- map["enabled_at"]
		disabled_at <- map["disabled_at"]
		deleted_at <- map["deleted_at"]
		created_at <- map["created_at"]
		updated_at <- map["updated_at"]
		logo <- map["logo"]
		balance <- map["balance"]
		group <- map["group"]
		marker <- map["marker"]
	}

}
