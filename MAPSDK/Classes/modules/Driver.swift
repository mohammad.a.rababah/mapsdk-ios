/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

public  struct Driver : Mappable {
	public var id : Int?
	public var status_id : Int?
	public var user_id : Int?
	public var group_id : Int?
	public var parent_id : Int?
	public var social_id : String?
	public var dob : String?
	public var gender : String?
	public var emergency_contact_name : String?
	public var emergency_contact_phone : String?
	public var ext_fee : String?
	public var min_orders : Int?
	public var max_orders : String?
	public var online : Int?
	public var away : Int?
	public var enabled : Int?
	public var disabled_reason : String?
	public var disabled_at : String?
	public var on_demand : Int?
	public var crowd : Int?
	public var auto_dispatch : Int?
	public var consolidated : Int?
	public var consolidated_crowd : Int?
	public var consolidated_auto_dispatch : Int?
	public var cropped_image : String?
	public var last_location_lat : String?
	public var last_location_lng : String?
	public var last_location_at : String?
	public var available_in : Int?
	public var app_ver : String?
	public var rating : Int?
	public var rating_count : Int?
	public var shift_id : Int?
	public var days_off : [String]?
	public var number_of_orders : Int?
	public var total_delivery_fees : Double?
	public var total_driver_fees : Double?
	public var total_actions : Int?
	public var total_transactions : Double?
	public var balance : Double?
	public var balance_after_tax : Double?
	public var total_value : Int?
	public var paid : Int?
	public var from : String?
	public var to : String?
	public var transport_method : String?
	public var transport_model : String?
	public var transport_brand : String?
	public var transport_engine : String?
	public var batch_timestamp : Int?
	public var created_at : String?
	public var updated_at : String?
	public var deleted_at : String?
	public var driver_org_photo : Driver_org_photo?
	public var driver_photo : Driver_photo?
	public var driver_license : Driver_license?
	public var last_location : Last_location?
	public var branch_groups_ids : [String]?
	public var status : Status?
	public var user : User?
	public var parent : Parent?
	public var branch_groups : [String]?
	public var marker : String?

public 	init?(map: Map) {

	}

	public mutating func mapping(map: Map) {

		id <- map["id"]
		status_id <- map["status_id"]
		user_id <- map["user_id"]
		group_id <- map["group_id"]
		parent_id <- map["parent_id"]
		social_id <- map["social_id"]
		dob <- map["dob"]
		gender <- map["gender"]
		emergency_contact_name <- map["emergency_contact_name"]
		emergency_contact_phone <- map["emergency_contact_phone"]
		ext_fee <- map["ext_fee"]
		min_orders <- map["min_orders"]
		max_orders <- map["max_orders"]
		online <- map["online"]
		away <- map["away"]
		enabled <- map["enabled"]
		disabled_reason <- map["disabled_reason"]
		disabled_at <- map["disabled_at"]
		on_demand <- map["on_demand"]
		crowd <- map["crowd"]
		auto_dispatch <- map["auto_dispatch"]
		consolidated <- map["consolidated"]
		consolidated_crowd <- map["consolidated_crowd"]
		consolidated_auto_dispatch <- map["consolidated_auto_dispatch"]
		cropped_image <- map["cropped_image"]
		last_location_lat <- map["last_location_lat"]
		last_location_lng <- map["last_location_lng"]
		last_location_at <- map["last_location_at"]
		available_in <- map["available_in"]
		app_ver <- map["app_ver"]
		rating <- map["rating"]
		rating_count <- map["rating_count"]
		shift_id <- map["shift_id"]
		days_off <- map["days_off"]
		number_of_orders <- map["number_of_orders"]
		total_delivery_fees <- map["total_delivery_fees"]
		total_driver_fees <- map["total_driver_fees"]
		total_actions <- map["total_actions"]
		total_transactions <- map["total_transactions"]
		balance <- map["balance"]
		balance_after_tax <- map["balance_after_tax"]
		total_value <- map["total_value"]
		paid <- map["paid"]
		from <- map["from"]
		to <- map["to"]
		transport_method <- map["transport_method"]
		transport_model <- map["transport_model"]
		transport_brand <- map["transport_brand"]
		transport_engine <- map["transport_engine"]
		batch_timestamp <- map["batch_timestamp"]
		created_at <- map["created_at"]
		updated_at <- map["updated_at"]
		deleted_at <- map["deleted_at"]
		driver_org_photo <- map["driver_org_photo"]
		driver_photo <- map["driver_photo"]
		driver_license <- map["driver_license"]
		last_location <- map["last_location"]
		branch_groups_ids <- map["branch_groups_ids"]
		status <- map["status"]
		user <- map["user"]
		parent <- map["parent"]
		branch_groups <- map["branch_groups"]
		marker <- map["marker"]
	}

}
