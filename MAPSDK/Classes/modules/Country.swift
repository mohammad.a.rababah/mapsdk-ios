/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

public struct Country : Mappable {
	public var id : Int?
	public var country_code : String?
	public var country_name_en : String?
	public var country_name_ar : String?
	public var timezone : String?
	public var iSD : String?
	public var phone_numbers : Int?
	public var phone_prefix : String?
	public var currency : String?
	public var flag : String?

	public init?(map: Map) {

	}

	public mutating func mapping(map: Map) {

		id <- map["id"]
		country_code <- map["country_code"]
		country_name_en <- map["country_name_en"]
		country_name_ar <- map["country_name_ar"]
		timezone <- map["timezone"]
		iSD <- map["ISD"]
		phone_numbers <- map["phone_numbers"]
		phone_prefix <- map["phone_prefix"]
		currency <- map["currency"]
		flag <- map["flag"]
	}

}
