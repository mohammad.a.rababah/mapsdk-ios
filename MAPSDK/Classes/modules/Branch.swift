/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

public struct Branch : Mappable {
	public var id : Int?
	public var client_id : Int?
	public var ingr_id : String?
	public var group_id : String?
	public var name : String?
	public var phone : String?
	public var country_id : Int?
	public var city_id : Int?
	public var area_id : Int?
	public var lat : String?
	public var lng : String?
	public var street : String?
	public var landmark : String?
	public var building : String?
	public var floor : Int?
	public var apartment : String?
	public var description : String?
	public var confirmed : Int?
	public var enabled : Int?
	public var reference_id : String?
	public var open : String?
	public var close : String?
	public var single : Int?
	public var deleted_at : String?
	public var created_at : String?
	public var updated_at : String?
	public var business_date : String?
	public var business_date_info : Business_date_info?
	public var business_hours_array : [String]?
	public var country : Country?
	public var city : City?
	public var area : Area?
	public var business_hours : [String]?

	public init?(map: Map) {

	}

	public mutating func mapping(map: Map) {

		id <- map["id"]
		client_id <- map["client_id"]
		ingr_id <- map["ingr_id"]
		group_id <- map["group_id"]
		name <- map["name"]
		phone <- map["phone"]
		country_id <- map["country_id"]
		city_id <- map["city_id"]
		area_id <- map["area_id"]
		lat <- map["lat"]
		lng <- map["lng"]
		street <- map["street"]
		landmark <- map["landmark"]
		building <- map["building"]
		floor <- map["floor"]
		apartment <- map["apartment"]
		description <- map["description"]
		confirmed <- map["confirmed"]
		enabled <- map["enabled"]
		reference_id <- map["reference_id"]
		open <- map["open"]
		close <- map["close"]
		single <- map["single"]
		deleted_at <- map["deleted_at"]
		created_at <- map["created_at"]
		updated_at <- map["updated_at"]
		business_date <- map["business_date"]
		business_date_info <- map["business_date_info"]
		business_hours_array <- map["business_hours_array"]
		country <- map["country"]
		city <- map["city"]
		area <- map["area"]
		business_hours <- map["business_hours"]
	}

}
