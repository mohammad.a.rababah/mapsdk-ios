/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

public  struct User : Mappable {
	public var id : Int?
	public var parent_id : Int?
	public var parent_access : Int?
	public var template_id : String?
	public var country_id : Int?
	public var city_id : String?
	public var first_name : String?
	public var last_name : String?
	public var email : String?
	public var phone : String?
	public var dob : String?
	public var gender : String?
	public var mac_address : String?
	public var sim_card_number : String?
	public var serial_number : String?
	public var jwt_token : String?
	public var fcm_token : String?
	public var maps_key : String?
	public var api_key : String?
	public var type : String?
	public var enabled : Int?
	public var enabled_at : String?
	public var disabled_at : String?
	public var online : Int?
	public var last_login_ip : String?
	public var last_login_country : String?
	public var last_login_country_code : String?
	public var last_login_at : String?
	public var created_at : String?
	public var updated_at : String?
	public var deleted_at : String?
	public var whatsapp_enabled : Int?
	public var verified : Int?
	public var confirmation_code : String?
	public var branding : String?
	public var photo : String?
	public var access : [String]?
	public var country : Country?

	public init?(map: Map) {

	}

	public mutating func mapping(map: Map) {

		id <- map["id"]
		parent_id <- map["parent_id"]
		parent_access <- map["parent_access"]
		template_id <- map["template_id"]
		country_id <- map["country_id"]
		city_id <- map["city_id"]
		first_name <- map["first_name"]
		last_name <- map["last_name"]
		email <- map["email"]
		phone <- map["phone"]
		dob <- map["dob"]
		gender <- map["gender"]
		mac_address <- map["mac_address"]
		sim_card_number <- map["sim_card_number"]
		serial_number <- map["serial_number"]
		jwt_token <- map["jwt_token"]
		fcm_token <- map["fcm_token"]
		maps_key <- map["maps_key"]
		api_key <- map["api_key"]
		type <- map["type"]
		enabled <- map["enabled"]
		enabled_at <- map["enabled_at"]
		disabled_at <- map["disabled_at"]
		online <- map["online"]
		last_login_ip <- map["last_login_ip"]
		last_login_country <- map["last_login_country"]
		last_login_country_code <- map["last_login_country_code"]
		last_login_at <- map["last_login_at"]
		created_at <- map["created_at"]
		updated_at <- map["updated_at"]
		deleted_at <- map["deleted_at"]
		whatsapp_enabled <- map["whatsapp_enabled"]
		verified <- map["verified"]
		confirmation_code <- map["confirmation_code"]
		branding <- map["branding"]
		photo <- map["photo"]
		access <- map["access"]
		country <- map["country"]
	}
    func toString() -> String{
        return "\(String(describing: first_name)) \(String(describing: last_name))"
    }
}
