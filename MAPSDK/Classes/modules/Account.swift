/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
import ObjectMapper

public  struct Account : Mappable {
 	public var id : Int?
 	public var user_id : Int?
	public var group_id : Int?
	public var team_leader_id : String?
	public var name : String?
	public var type : String?
	public var enabled : Int?
	public var no_address : Int?
	public var only_wallet : Int?
	public var no_name : Int?
	public var no_customer : Int?
	public var address_search : Int?
	public var allow_partial_pay : Int?
	public var show_cancel : Int?
	public var show_fees : Int?
	public var online : Int?
	public var mac_address : String?
	public var lat : String?
	public var lng : String?
	public var maps_key : String?
	public var api_key : String?
	public var pickup_handling_time : Int?
	public var dropoff_handling_time : Int?
	public var buffer_time : Int?
	public var surge_multiplier : String?
	public var additional_fees : String?
	public var fees_calculations : String?
	public var custom_fees : String?
	public var custom_fees_details : String?
	public var base_fare : Int?
	public var cost_per_minute : Int?
	public var cost_per_km : Int?
	public var partial_pay : String?
	public var min_preperation_time : Int?
	public var app_ver : String?
	public var created_at : String?
	public var updated_at : String?
	public var deleted_at : String?
	public var logo : String?
	public var user : User?

	public  init?(map: Map) {

	}

	public mutating func mapping(map: Map) {

		id <- map["id"]
		user_id <- map["user_id"]
		group_id <- map["group_id"]
		team_leader_id <- map["team_leader_id"]
		name <- map["name"]
		type <- map["type"]
		enabled <- map["enabled"]
		no_address <- map["no_address"]
		only_wallet <- map["only_wallet"]
		no_name <- map["no_name"]
		no_customer <- map["no_customer"]
		address_search <- map["address_search"]
		allow_partial_pay <- map["allow_partial_pay"]
		show_cancel <- map["show_cancel"]
		show_fees <- map["show_fees"]
		online <- map["online"]
		mac_address <- map["mac_address"]
		lat <- map["lat"]
		lng <- map["lng"]
		maps_key <- map["maps_key"]
		api_key <- map["api_key"]
		pickup_handling_time <- map["pickup_handling_time"]
		dropoff_handling_time <- map["dropoff_handling_time"]
		buffer_time <- map["buffer_time"]
		surge_multiplier <- map["surge_multiplier"]
		additional_fees <- map["additional_fees"]
		fees_calculations <- map["fees_calculations"]
		custom_fees <- map["custom_fees"]
		custom_fees_details <- map["custom_fees_details"]
		base_fare <- map["base_fare"]
		cost_per_minute <- map["cost_per_minute"]
		cost_per_km <- map["cost_per_km"]
		partial_pay <- map["partial_pay"]
		min_preperation_time <- map["min_preperation_time"]
		app_ver <- map["app_ver"]
		created_at <- map["created_at"]
		updated_at <- map["updated_at"]
		deleted_at <- map["deleted_at"]
		logo <- map["logo"]
		user <- map["user"]
	}

}
