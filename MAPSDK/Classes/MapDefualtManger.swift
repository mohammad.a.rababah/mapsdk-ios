//
//  UserDefualtManger.swift
//  Driver
//
//  Created by Tikino Pawn on 6/12/19.
//  Copyright © 2019 Yallow. All rights reserved.
//

import Foundation
class YallowUserDefualtManger {
    private let userDefualt = UserDefaults.standard
    private let PROFILE_KEY = "Profile_key"
    private static let YALLOW_USER_DEFUALT_MANGER = YallowUserDefualtManger()
  
    private init(){}
    
    static func  getIntsance()-> YallowUserDefualtManger{
        return YALLOW_USER_DEFUALT_MANGER
    }
    func  saveBrand(profile: Brand?){
        if(profile == nil)
        {return
        }
        let encodedData  = NSKeyedArchiver.archivedData(withRootObject: profile!)
        userDefualt.set(encodedData, forKey: PROFILE_KEY)
         userDefualt.synchronize()
    }
    func  loadProfile()-> Brand?{
        let decoded  = userDefualt.data(forKey:PROFILE_KEY)
        if(decoded == nil)
        {return nil
            
        }
        let profile = NSKeyedUnarchiver.unarchiveObject(with: decoded!) as! Brand
        return profile
    }
}
