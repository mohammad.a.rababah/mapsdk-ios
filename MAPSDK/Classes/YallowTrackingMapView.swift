//
//  MapView.swift
//  MapTest
//
//  Created by Tikino Pawn on 8/10/20.
//  Copyright © 2020 Yallow. All rights reserved.
//

import UIKit
import GoogleMaps
import FirebaseCore
import FirebaseDatabase
import Kingfisher
import Alamofire
import SwiftyJSON


public class YallowTrackingMapView: GMSMapView {
    private static var  apiKey = ""
    private static let googleApiKey = "AIzaSyAbIEa6Ooo9KBjgFhT2STF7LlaIeuLR9lE"
    private static var  brand = Brand()
    static var orderId = -1
   public static  var isInited = false
    private var isFirstOrderRequest = true
    private var isFirstDriverRequest = true
    var loadImageFromUrl = false
    private static let APP_NAME = "YallowMap"
    var pickupImage = UIImage(named: "shop_pin")
    var customerImage = UIImage(named: "home_pin")
    var driverImage = UIImage(named: "driver_pin")
    private var driverMarker  : GMSMarker? = nil
    private var customerMarker : GMSMarker? = nil
    private var shopMarker  : GMSMarker? = nil
    public var showMarkerfromUrl = true
public var mapPadding = CGFloat(0)
    var markerList : [GMSMarker] = []
    public var yallowOrderDeleget: YallowOrderDeleget!
    
  public   static func initMap(apiKey : String, yallowDeleget :YallowMapSDKDeleget?){
        YallowTrackingMapView.apiKey = apiKey
        loadBrand(yallowDeleget)
        GMSServices.provideAPIKey(googleApiKey)
        
      
     
    }
 public  func trackOrder(orderId : Int, yallowOrderDeleget : YallowOrderDeleget ){
        self.yallowOrderDeleget = yallowOrderDeleget
        YallowTrackingMapView.orderId = orderId
        MapResfullApi.getIntsance().loadOrderDetails(apiKey: YallowTrackingMapView.apiKey,order_id: String(YallowTrackingMapView.orderId),loginResponse: {res in
                  if(res.response?.statusCode == 200){
                    self.onOrderRecived(order: res.value!)
                    yallowOrderDeleget.onOrderUpdate(order: res.value!)
                  }else{
                    yallowOrderDeleget.onOrderFail(message: res.error?.localizedDescription ?? "error read order")
            }
                  
        })
        
    }
 
    
    
    private static func loadBrand(_ yallowDeleget :YallowMapSDKDeleget?){
        MapResfullApi.getIntsance().loadBrand(apiKey: apiKey, loginResponse: {res in
            if(res.response?.statusCode == 200){
                self.isInited =  true
                self.brand = res.value ?? Brand()
                print(self.brand)
                let options = FirebaseOptions(googleAppID: "1:17056804964:ios:25859b4236612c4597af71" , gcmSenderID: "17056804964")
                options.bundleID = "com.yallow.MapSDK"
                options.projectID = "yallow-93ccf"
                options.apiKey = "AIzaSyAbIEa6Ooo9KBjgFhT2STF7LlaIeuLR9lE"
                options.databaseURL = brand.FB_database_url
              FirebaseApp.configure(options: options)
              isInited = true
                yallowDeleget?.onInitSuccessfully()

            }else{
                 isInited = false
                yallowDeleget?.onInitFail(message: res.error?.localizedDescription ?? "unable to load Brand")
                        
            }})
    }
    
    private func onOrderRecived(order : OrderResponse){
        if(self.isFirstOrderRequest){
            
            Database.database(url: YallowTrackingMapView.brand.FB_database_url).reference().child("branches").child(String(order.branch?.id ?? -1)).child(String(order.id ?? -1)).observe(.value, with: {res in
                if(self.isFirstOrderRequest){
                    self.isFirstOrderRequest = false
                }else{
                    self.trackOrder(orderId : order.id ?? -1, yallowOrderDeleget : self.yallowOrderDeleget)
                    
                }
            })
            
        }
         if(order.branch != nil){
        loadBranchMarker(order)
        }
        if(order.customer != nil && order.customer_address != nil){
        loadCustomerMarker(order)
        }
       if(order.driver != nil){
            readDriverFirebase(order)
            loadDriverMarker(order)
        }
       
        
        if(driverMarker != nil)
        {

            markerList.append(driverMarker!)
        }
        if(customerMarker != nil)
        {   markerList.append(customerMarker!)
            
        }
        if(shopMarker != nil){
            markerList.append(shopMarker!)
                
            }
        if(isFirstDriverRequest){
        isFirstDriverRequest = false
        resetCamera()
        }
    }
    public func resetCamera(){
         var bounds = GMSCoordinateBounds()
        if(markerList.count >= 2){
            for marker in markerList {
                bounds = bounds.includingCoordinate(marker.position)
            }
            let update = GMSCameraUpdate.fit(bounds, withPadding: mapPadding)
                animate(with: update)
        
            }
            else if(markerList.count == 1){
                animate(to: GMSCameraPosition(target:  markerList[0].position, zoom: 12))
               
            }

    }
    func loadCustomerMarker(_ order : OrderResponse){
          let postion =  CLLocationCoordinate2D(latitude: Double(order.customer_address!.lat!)!, longitude: Double(order.customer_address!.lng!)!)
        if(customerMarker == nil){
      customerMarker = GMSMarker()
        customerMarker?.position = postion
        customerMarker?.title = order.customer?.name
        customerMarker?.icon = customerImage
        loadImageToMarker(customerMarker, order.customer_address?.marker)
        customerMarker?.map = self
        }else{
            customerMarker?.position = postion

        }

    }
    
    func loadBranchMarker(_ order : OrderResponse){
             let postion =  CLLocationCoordinate2D(latitude: Double(order.branch!.lat!)!, longitude: Double(order.branch!.lng!)!)
           if(shopMarker == nil){
         shopMarker = GMSMarker()

           shopMarker?.position = postion
           shopMarker?.title = order.branch?.name
           shopMarker?.icon = pickupImage
            loadImageToMarker(shopMarker, order.client?.marker)
           shopMarker?.map = self
           }else{
               shopMarker?.position = postion

           }

       }
    
    func loadDriverMarker(_ order : OrderResponse){
        if(order.driver?.last_location?.lat == nil  || order.driver?.last_location?.lng == nil){
        return
        }
        
        
        let postion =  CLLocationCoordinate2D(latitude: Double(order.driver!.last_location!.lat!)!, longitude: Double(order.driver!.last_location!.lng!)!)
        if(driverMarker == nil){
            driverMarker = GMSMarker()
            self.yallowOrderDeleget.onDriverLocationUpdate(location: postion )
            driverMarker?.position = postion
            driverMarker?.title = order.driver!.user!.toString()
            driverMarker?.icon = driverImage
            loadImageToMarker(driverMarker, order.driver!.marker)
            driverMarker?.map = self
            
        }else{
            
            CATransaction.begin()
            CATransaction.setAnimationDuration(10.0)
            driverMarker?.position = postion
            CATransaction.commit()
            
                  

              }

          }
        
    
    
    func loadImageToMarker(_ marker : GMSMarker?,_ url : String?){
        if(url == nil || !showMarkerfromUrl){
            return
        }
         let url = URL(string: url!)!
        
        URLSession.shared.dataTask(with: url,completionHandler: {(data,urlResponse,error) in
            DispatchQueue.main.async(execute: {
                marker?.icon = UIImage(data: data!,scale: 3.0)
           })
        })
    

//        KingfisherManager.shared.retrieveImage(with: url, options: nil, progressBlock: nil, completionHandler:{ image, error, cacheType, imageURL in
//
//            marker?.icon = self.reSizeImage(image: image!, scaledToSize: CGSize(width: image!.size.width / 2.5, height: (image?.size.height)! / 2.5))

//        })
        
      
        
      
    }
    func readDriverFirebase(_ order : OrderResponse){
        Database.database(url: YallowTrackingMapView.brand.FB_database_url).reference().child("drivers").child(String(order.driver!.id!)).child("location").observe(.value, with: {res in
            let value = res.value as? [String : AnyObject] ?? [:]
            if( value["lat"] != nil){
            var driverOrder = order
           driverOrder.driver!.last_location!.lat = value["lat"] as? String
           driverOrder.driver!.last_location!.lng = value["lng"] as? String
            self.loadDriverMarker(driverOrder)
            }
           
            
            
        })
    }
    func reSizeImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        image.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
}




public protocol YallowMapSDKDeleget {
    func onInitSuccessfully()
    func onInitFail(message:String)
}


public protocol YallowOrderDeleget {
    func onOrderUpdate(order: OrderResponse)
    func onDriverLocationUpdate(location: CLLocationCoordinate2D)
    func onOrderFail(message:String)
}

