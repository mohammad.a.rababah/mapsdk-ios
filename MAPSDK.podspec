#
# Be sure to run `pod lib lint MAPSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'MAPSDK'
  s.version          = '0.0.19'
  s.summary          = 'A short description of MAPSDK.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://github.com/YallowApp/MAPSDK'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'mohammad.a.rababah' => 'mohammad.a.rababah@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/mohammad.a.rababah/mapsdk-ios.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'

  s.source_files = 'MAPSDK/Classes/**/*'
  
  # s.resource_bundles = {
  #   'MAPSDK' => ['MAPSDK/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  s.static_framework = true
  s.dependency  'Alamofire', '~> 4.1'
  s.dependency  'AlamofireObjectMapper', '~> 5.2'
  s.dependency  'AlamofireNetworkActivityLogger', '~> 2.0'
  s.dependency  'GoogleMaps'
  s.dependency  'Firebase/Database'
  s.dependency  'Firebase/Messaging'
  s.dependency  'Firebase/Core'
  s.dependency  'Firebase'
  s.dependency  'Kingfisher'
  s.dependency  'SwiftyJSON', '~> 4.2'
end
